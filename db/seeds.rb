# coding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'csv'

# csvファイルから犬種テーブルデータ作成
Breed.delete_all unless Breed.count.zero?
CSV.foreach('db/breeds.csv', encoding: 'Shift_JIS:UTF-8') do |row|
  Breed.create!(:name => row[0], :size_id => row[1])
end

# csvファイルから都道府県テーブルデータ作成
Pref.delete_all unless Pref.count.zero?
CSV.foreach('db/pref.csv') do |row|
  Pref.create(name: row[1])
end

# # 性別、サイズ、コース、履歴区分
Size.delete_all unless Size.count.zero?
Size.create(id: 1, name: '超小型犬')
Size.create(id: 2, name: '小型犬')
Size.create(id: 3, name: '中型犬')
Size.create(id: 4, name: '大型犬')
Size.create(id: 5, name: '超大型犬')

Sex.delete_all unless Sex.count.zero?
Sex.create(id: 1, name: '男の子')
Sex.create(id: 2, name: '女の子')
Sex.create(id: 3, name: '男の子（去勢済み）')
Sex.create(id: 4, name: '女の子（去勢済み）')
Sex.create(id: 9, name: '不明')

Course.delete_all unless Course.count.zero?
Course.create(id: 1, size_id: 1, name: '超小型犬／全身カットコース', price: 6000)
Course.create(id: 2, size_id: 1, name: '超小型犬／部分カットコース', price: 5000)
Course.create(id: 3, size_id: 1, name: '超小型犬／シャンプーコース', price: 4000)
Course.create(id: 4, size_id: 2, name: '小型犬／全身カットコース', price: 7000)
Course.create(id: 5, size_id: 2, name: '小型犬／部分カットコース', price: 6000)
Course.create(id: 6, size_id: 2, name: '小型犬／シャンプーコース', price: 5000)
Course.create(id: 7, size_id: 3, name: '中型犬／全身カットコース', price: 8000)
Course.create(id: 8, size_id: 3, name: '中型犬／部分カットコース', price: 7000)
Course.create(id: 9, size_id: 3, name: '中型犬／シャンプーコース', price: 6000)

HistoryCategory.delete_all unless HistoryCategory.count.zero?
HistoryCategory.create(id: 1, name: '予約')
HistoryCategory.create(id: 2, name: '終了')
HistoryCategory.create(id: 3, name: 'キャンセル')