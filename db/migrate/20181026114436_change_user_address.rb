class ChangeUserAddress < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :postal, :string
    add_column :users, :pref_id, :integer
    add_column :users, :city, :string
    add_column :users, :address_1, :string
    rename_column :users, :address, :address_2
    remove_column :users, :tel, :integer
    add_column :users, :tel, :string
  end
end
