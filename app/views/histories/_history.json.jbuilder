json.extract! history, :id, :dog_id, :date, :course_id, :memo, :cut_image, :created_at, :updated_at
json.url history_url(history, format: :json)
