class RenameSizeToBreed < ActiveRecord::Migration[5.2]
  def change
    rename_column :breeds, :size, :size_id
  end
end
