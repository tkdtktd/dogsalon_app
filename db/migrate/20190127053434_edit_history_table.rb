class EditHistoryTable < ActiveRecord::Migration[5.2]
  def change
    add_column :histories, :start_time, :time
    add_column :histories, :end_time, :time
    add_column :histories, :history_category_id, :integer
  end
end
