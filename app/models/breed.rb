class Breed < ApplicationRecord
  has_many :dogs
  belongs_to :size

  mount_uploader :breed_img, BreedImgUploader

  # id順、名前順に並び替える
  default_scope { order(id: :asc, name: :asc) }
end
