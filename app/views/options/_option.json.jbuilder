json.extract! option, :id, :history_id, :course_id, :created_at, :updated_at
json.url option_url(option, format: :json)
