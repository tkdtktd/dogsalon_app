class Dog < ApplicationRecord
  belongs_to :user
  belongs_to :breed
  belongs_to :sex
  has_many :histories
  validates :user_id, presence: true
  validates :name, presence: true
  validates :breed_id, presence: true
  # validates :sex_id, presence: true
  # validates :date, presence: true

  mount_uploader :dog_image, DogImageUploader

end
