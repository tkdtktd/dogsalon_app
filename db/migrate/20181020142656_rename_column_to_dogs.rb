class RenameColumnToDogs < ActiveRecord::Migration[5.2]
  def change
    rename_column :dogs, :sex, :sex_id
  end
end
