class AddExplainToBreed < ActiveRecord::Migration[5.2]
  def change
    add_column :breeds, :explain, :text
  end
end
