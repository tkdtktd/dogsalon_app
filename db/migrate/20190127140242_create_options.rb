class CreateOptions < ActiveRecord::Migration[5.2]
  def change
    create_table :options do |t|
      t.integer :history_id
      t.integer :course_id

      t.timestamps
    end
  end
end
