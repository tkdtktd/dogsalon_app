class CreateDogs < ActiveRecord::Migration[5.2]
  def change
    create_table :dogs do |t|
      t.integer :user_id
      t.string :name
      t.integer :breed_id
      t.integer :sex
      t.date :birthday
      t.text :memo
      t.string :dog_image

      t.timestamps
    end
  end
end
