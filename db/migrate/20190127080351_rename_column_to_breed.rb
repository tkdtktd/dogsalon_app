class RenameColumnToBreed < ActiveRecord::Migration[5.2]
  def change
    rename_column :breeds, :explain, :memo
  end
end
