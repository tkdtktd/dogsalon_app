json.extract! breed, :id, :name, :size, :created_at, :updated_at
json.url breed_url(breed, format: :json)
