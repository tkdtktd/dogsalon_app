class AddSizeIdToCourse < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :size_id, :integer
  end
end
