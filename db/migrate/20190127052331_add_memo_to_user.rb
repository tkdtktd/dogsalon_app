class AddMemoToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :memo, :text
    rename_column :users, :admin_flg, :is_admin
  end
end
