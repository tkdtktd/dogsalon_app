class CreateHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :histories do |t|
      t.integer :dog_id
      t.date :date
      t.integer :course_id
      t.text :memo
      t.string :cut_image

      t.timestamps
    end
  end
end
