class Size < ApplicationRecord
  has_many :breeds
  has_many :courses
end
