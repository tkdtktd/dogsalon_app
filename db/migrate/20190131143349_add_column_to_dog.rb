class AddColumnToDog < ActiveRecord::Migration[5.2]
  def change
    add_column :dogs, :is_fixed, :boolean
  end
end
