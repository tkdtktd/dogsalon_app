class AddNameKanaToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :name_kana, :string
    add_column :users, :sex_id, :integer
  end
end