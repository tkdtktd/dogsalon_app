json.extract! history_category, :id, :name, :created_at, :updated_at
json.url history_category_url(history_category, format: :json)
