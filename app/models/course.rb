class Course < ApplicationRecord
  has_many :histories
  has_many :options
  belongs_to :size
end
