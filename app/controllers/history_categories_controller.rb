class HistoryCategoriesController < ApplicationController
  before_action :set_history_category, only: [:show, :edit, :update, :destroy]

  # GET /history_categories
  # GET /history_categories.json
  def index
    @history_categories = HistoryCategory.all
  end

  # GET /history_categories/1
  # GET /history_categories/1.json
  def show
  end

  # GET /history_categories/new
  def new
    @history_category = HistoryCategory.new
  end

  # GET /history_categories/1/edit
  def edit
  end

  # POST /history_categories
  # POST /history_categories.json
  def create
    @history_category = HistoryCategory.new(history_category_params)

    respond_to do |format|
      if @history_category.save
        format.html { redirect_to @history_category, notice: 'History category was successfully created.' }
        format.json { render :show, status: :created, location: @history_category }
      else
        format.html { render :new }
        format.json { render json: @history_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /history_categories/1
  # PATCH/PUT /history_categories/1.json
  def update
    respond_to do |format|
      if @history_category.update(history_category_params)
        format.html { redirect_to @history_category, notice: 'History category was successfully updated.' }
        format.json { render :show, status: :ok, location: @history_category }
      else
        format.html { render :edit }
        format.json { render json: @history_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /history_categories/1
  # DELETE /history_categories/1.json
  def destroy
    @history_category.destroy
    respond_to do |format|
      format.html { redirect_to history_categories_url, notice: 'History category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_history_category
      @history_category = HistoryCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def history_category_params
      params.require(:history_category).permit(:name)
    end
end
