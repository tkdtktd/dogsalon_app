class ApplicationController < ActionController::Base
  # Deviseで使用するパラメータをカスタマイズ
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    # sign_up、updateの時の入力項目を追加する。
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name, :postal, :pref_id, :city, :address_1, :address_2, :tel])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :postal, :pref_id, :city, :address_1, :address_2, :tel])
  end

end