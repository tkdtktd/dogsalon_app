Rails.application.routes.draw do
  resources :history_categories
  resources :options
  get 'home/top'
  # 管理画面の設定
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  resources :sizes
  resources :sexes
  devise_for :users, module: :users, controllers: {registrations: 'users/registrations'}
  # devise が生成してくれたownerモデルのコントローラのうち、registrations_controller はこちらでカスタムしたものを使いますよ、という宣言。（registrationsのすべてのメソッドを定義する必要はなく、カスタムしたいものだけを宣言すればよい）
  resources :users, only: [:index, :show]
  resources :courses
  resources :breeds
  resources :histories
  resources :dogs
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'home#top'
end
