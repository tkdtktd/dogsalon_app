class History < ApplicationRecord
  belongs_to :dog
  belongs_to :course
  belongs_to :history_category

  has_many :options

  mount_uploader :cut_image, CutImageUploader
end
