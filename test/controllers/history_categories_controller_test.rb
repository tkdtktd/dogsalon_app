require 'test_helper'

class HistoryCategoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @history_category = history_categories(:one)
  end

  test "should get index" do
    get history_categories_url
    assert_response :success
  end

  test "should get new" do
    get new_history_category_url
    assert_response :success
  end

  test "should create history_category" do
    assert_difference('HistoryCategory.count') do
      post history_categories_url, params: { history_category: { name: @history_category.name } }
    end

    assert_redirected_to history_category_url(HistoryCategory.last)
  end

  test "should show history_category" do
    get history_category_url(@history_category)
    assert_response :success
  end

  test "should get edit" do
    get edit_history_category_url(@history_category)
    assert_response :success
  end

  test "should update history_category" do
    patch history_category_url(@history_category), params: { history_category: { name: @history_category.name } }
    assert_redirected_to history_category_url(@history_category)
  end

  test "should destroy history_category" do
    assert_difference('HistoryCategory.count', -1) do
      delete history_category_url(@history_category)
    end

    assert_redirected_to history_categories_url
  end
end
